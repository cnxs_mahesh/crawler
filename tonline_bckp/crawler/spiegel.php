<?php
//this script gets the links for every article on spiegel.
function getHtml($url){
	global $useragents;
        $user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
        $options = array(CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
        CURLOPT_POST => false, //set to GET
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 8, // timeout on connect
        CURLOPT_TIMEOUT => 8, // timeout on response
        CURLOPT_MAXREDIRS => 2, // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
        $response = $header['content'];
		extractData($response, $url);
        return $header['content'];
}


function getArticleLinks($url){
	

        $user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
        $options = array(CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
        CURLOPT_POST => false, //set to GET
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 8, // timeout on connect
        CURLOPT_TIMEOUT => 8, // timeout on response
        CURLOPT_MAXREDIRS => 2, // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
        
        $response = $header['content'];
        
		$lines = explode("\n", $response);
		$articles = array();
		foreach($lines as $line){
			if(strpos($line, '<span class="news-archive-headline-intro">')){
				//ok, this line has an article link in it.
				preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $line, $result);
				$articles[] = $result['href'][0];
			}
		}

        return $articles;
}

//firstly we need to build the archive index, this is every page that contains every article 
$stem = "http://www.spiegel.de/nachrichtenarchiv/artikel-";
//we start at january 1st 2000.
$start = strtotime("1st January 2017");
$endTime = time();
//946684800;

if($endTime>time()){
	$endTime = time();
}

for($time=$start; $time<$endTime; $time=$time+86400){
	//echo $stem . date("d.m.Y", $time).".html" ."\n";
	$articles = getArticleLinks($stem . date("d.m.Y", $time).".html");
	file_put_contents("spiegel/articles.txt", implode("\n", $articles), FILE_APPEND);
	
}
