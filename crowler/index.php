<!DOCTYPE html>
<html>
 <head>
  <title>Web Crawler of T-online</title>
 </head>
 <body>
  <div id="content" style="margin-top:10px;height:100%;">
   <center><h1>Links from T-online</h1></center>

   <br/>
<?php include('../scrapper/index.php') ?>
   <?php
   libxml_use_internal_errors(TRUE); //disable libxml errors
   
   $url_array=array();
   include("simple_html_dom.php");
   $crawled_urls=array();
   $found_urls=array();
   function rel2abs($rel, $base){
    if (parse_url($rel, PHP_URL_SCHEME) != '') return $rel;
    if (isset($rel[0]) && ( $rel[0]=='#' || $rel[0]=='?')) return $base.$rel;
    extract(parse_url($base));
    $path = preg_replace('#/[^/]*$#', '', $path);
    if (isset($rel[0]) && $rel[0] == '/') $path = '';
    $abs = "$host$path/$rel";
    $re = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');
    for($n=1; $n>0; $abs=preg_replace($re,'/', $abs,-1,$n)){}
    $abs=str_replace("../","",$abs);
    return $scheme.'://'.$abs;
   }
   function perfect_url($u,$b){
    $bp=parse_url($b);
    if(($bp['path']!="/" && $bp['path']!="") || $bp['path']==''){
     if($bp['scheme']==""){$scheme="http";}else{$scheme=$bp['scheme'];}
     $b=$scheme."://".$bp['host']."/";
    }
    if(substr($u,0,2)=="//"){
     $u="http:".$u;
    }
    if(substr($u,0,4)!="http"){
     $u=rel2abs($u,$b);
    }
    return $u;
   }
   function crawl_site($u){
    global $crawled_urls;
    $uen=urlencode($u);
    if((array_key_exists($uen,$crawled_urls)==0 || $crawled_urls[$uen] < date("YmdHis",strtotime('-25 seconds', time())))){
     $html = file_get_html($u);
     $crawled_urls[$uen]=date("YmdHis");
     foreach($html->find("a") as $li){
      $url=perfect_url($li->href,$u);
      $enurl=urlencode($url);
      if($url!='' && substr($url,0,4)!="mail" && substr($url,0,4)!="java"){
        $found_urls[$enurl]=1;


        $GLOBALS['url_array'][]=$url;
        echo "<li><a target='_blank' href='".$url."'>".$url."</a></li>";
        fetchData($url);
      }
      //$url_data = json_encode(array('urls' => $found_urls));
      
     }
     //echo "<pre>";print_r($url_array);echo "</pre>";
    }
   }
   $url="http://www.t-online.de/";
    crawl_site($url);
    //$vars['links']=$url_array;
    //file_put_contents('results.json', json_encode($vars));
   ?>
  </div>
  <style>
  input{
   border:none;
   padding:8px;
  }
  </style>
 </body>
</html>
