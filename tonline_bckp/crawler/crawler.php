<?php
#!/usr/bin/php

global $time;
global $links_all;
global $crowledUrl;
global $logfile;
$links_all = array();
$crowledUrl = array();
$count = 0;
$time = time();

$links_all[] = $argv[1];


function getDataString($startString, $endString, $html){
	$startPos = strpos($html, $startString, strlen($startString));
	$endPos = strpos($html, $endString, $startPos);
	if($startPos>0&&$endPos>0){
		return trim(strip_tags(substr($html, $startPos, $endPos-$startPos)));
	}else{
		return null;		
	}
}

function extractData($html, $url){
	// BREADCRUMB
	global $time;
	$result = array();
	$result['url'] = $url;
	
	$articleIntro = getDataString('<p class="article-intro">', '</p>', $html);
	$result['articleIntro'] = $articleIntro;
	
	$author = getDataString('<a class="autor-link js-author-link"', '</a>', $html);
	$result['author'] = $author;
	
	//GET TIME OF ARTICLE
	$startString = 'datetime="';
	$startPos = strpos($html, $startString);
	$endPos = strpos($html, '">', $startPos);
	if($startPos>0&&$endPos>0){
		$result['datetime'] = substr($html, $startPos+ strlen($startString), $endPos-(($startPos + strlen($startString))));
	}else{
		$result['datetime'] = null;
	}
	
	//GET CONTENT OF ARTICLE
	//for this we have to get a subset of the article
	$startString = '<time class="timeformat"';
	$startPos = strpos($html, $startString);
	$endPos = strpos($html, '<div class="module-box home-link-box">', $startPos);
	$subHtml = substr($html, $startPos+ strlen($startString), $endPos-(($startPos + strlen($startString))));
	
	$content = array();
	$doc = new DOMDocument();
	@$doc->loadHTML($subHtml);
	foreach($doc->getElementsByTagName('p') as $p){
		$content[] = utf8_decode(trim($p->textContent))."\n";
	}
	array_shift($content);
	$result['content'] = implode("\n",$content);

/*mahesh code begins*/
    $html = file_get_contents($url);
    $doc->loadHTML($html);
    $xpath = new DOMXPath($doc);
    $query = '//*/meta[starts-with(@property, \'og:\')]';
    $metas = $xpath->query($query);
    foreach ($metas as $meta) {
        $meta_array[$meta->getAttribute('property')]=$meta->getAttribute('content');
    }
    if(!empty($meta_array))
    {
        $result['url']=$meta_array['og:url'];
        $result['headline']=$meta_array['og:title'];
        $result['timestamp']=strtotime($result['datetime']);
        $result['img']=$meta_array['og:image'];
    }
/*mahesh code end*/
	
	

	if($result['content']&&$result['datetime']){
		file_put_contents('spiegel/'.$time.".json", json_encode($result) ."\n", FILE_APPEND);
	}
	
	// else{
		// file_put_contents("spiegel/___FAIL__".md5($url), $url."\n".$html);
	// }
		
}

/*
 * @debugging purpose
 * 
 * 
 * echo shell_exec('php -i | grep curl');
 * echo shell_exec('php -r "curl_init();"');
 * echo shell_exec('php -v');
 * echo shell_exec('/usr/bin/php -v');
 * echo shell_exec('php --ini');
 */

declare(ticks = 1);
//create a new log file using domain name 
$logfileName = get_domain($argv[1]);
//unlink previous logfile if already created
@unlink($logfileName.".log");
$logfile = fopen($logfileName.".log", "a") or die("Unable to open file!");

pcntl_signal(SIGTERM, "signal_handler");
pcntl_signal(SIGINT, "signal_handler");

function signal_handler($signal) {
    global $logfile;
    switch($signal) {
        case SIGTERM:
            print "Caught SIGTERM\n";
            exit;
        case SIGKILL:
            print "Caught SIGKILL\n";
            exit;
        case SIGINT:
            print "Caught SIGINT\n";
            fwrite($logfile, "Caught SIGKILL\n");
            fclose($logfile);
            exit;
    }
}
do {
   // echo "size of array === ".sizeof($links_all)."\n";
    if (sizeof($links_all) > 0 && ! in_array($links_all[$count], $crowledUrl)) {
        crawlSite($links_all[$count]);
        //now log each link, which completed crowling
        fwrite($logfile, $links_all[$count]."\n");
        $crowledUrl[] = $links_all[$count];
    } else {
        print("Please specify a url for crawl OR Already crawled URL supplied");
        print("\n");
    }

    if(sizeof($links_all) < $count){
        //if such condition will happen then reset the count
        $count = 0;
    }else{
        array_splice($links_all, $count, 1);
        $count++;    
    }    
    if(sizeof($links_all) === 0){
        print("Given Url and suburls is crowled completely. All is done.\n");
        //now close the logger file
        fclose($logfile);
        die("Bye for now! \n");
    }    
} while (count($links_all) > 0);


    
/**
 * @param String $url
 * @desc call initial crawl, and used to make subsequent calls
 * @return array
 */
function crawlSite($url){
    print("Crawling site: " . $url);
    print("\n");
    if (filter_var(trim($url), FILTER_VALIDATE_URL) === false) {
        print("Please provide a valid url. Example http://example.com");
        print "\n";
    }
    $links = scrapLinks(getHtml($url), $url);
}


/**
 * @param string $url
 * @return mixed
 * @uses CURL
 * @desc scraps HTML of $url
 * returns it for the further use 
 * 
 */
function getHtmlOld($url){
    $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml, text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $header[] = "Cache-Control: max-age=0";
    $header[] = "Connection: keep-alive";
    $header[] = "Keep-Alive: 300";
    $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $header[] = "Accept-Language: en-us,en;q=0.5";
    
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Ubuntu/10.04 Chromium/6.0.472.53 Chrome/6.0.472.53 Safari/534.3');
    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // very important to set it to true, otherwise the content will be not be saved to string
    $html = curl_exec($curl); // execute the curl command
    return $html;   
    
}

function getHtml_NEW($url){
    $arrContextOptions=array(
        "ssl"=>array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    ); 
    $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
    return $response;    
}

// function getHtml($url){
    // $arrContextOptions=array(
        // "ssl"=>array(
            // "verify_peer"=>false,
            // "verify_peer_name"=>false,
        // ),
    // ); 
    // $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
	// extractData($response, $url);
    // return $response;    
// }

function getHtml($url){
	global $useragents;
        $user_agent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";
        $options = array(CURLOPT_CUSTOMREQUEST => "GET", //set request type post or get
        CURLOPT_POST => false, //set to GET
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 8, // timeout on connect
        CURLOPT_TIMEOUT => 8, // timeout on response
        CURLOPT_MAXREDIRS => 2, // stop after 10 redirects
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header['errno'] = $err;
        $header['errmsg'] = $errmsg;
        $header['content'] = $content;
        $response = $header['content'];
		extractData($response, $url);
        return $header['content'];
}


/**
 * @param mixed $html
 * @param string $url
 * @desc scrpas links from the given HTML
 * @uses DOMDocument
 * @return bool
 */
function scrapLinks($html, $url){
    global $crowledUrl;
    global $links_all;
    
    $pieces = parse_url($url);
    $dom = new DOMDocument();
    @$dom->loadHTML($html);
    $links = $dom->getElementsByTagName('a');
    
    foreach ($links as $link) {
        $href = parse_url($link->getAttribute('href'));
        if (isset($href['path']) && strpos($href['path'], 'void(0)') === false && ! in_array($link->getAttribute('href'), $crowledUrl)) {
            $newLink = rel2abs($link->getAttribute('href'), $url);
            if (strpos($newLink, $pieces['host']) !== false) {
                $links_all[] = $newLink;
            }
        }
    }
    return;
}

/**
 * @param string $rel
 * @param string $base
 * @return string string
 * @desc used to convert relative path to absolutes
 */
function rel2abs($rel, $base)
{
    $relUrl = parse_url($rel);
    $path = '';
    if (isset($relUrl['scheme']) && $relUrl['scheme'] != '' && isset($relUrl['host']) && $relUrl['host'] != '' && isset($relUrl['path']) && $relUrl['path'] != '')
        return $rel;
    else if (isset($relUrl['host']) && $relUrl['host'] != '' && ! isset($relUrl['scheme']) && ! isset($relUrl['path']))
        return "http://" . $relUrl['host'];
    else if (isset($relUrl['path']) && $relUrl['path'] != '' && ! isset($relUrl['scheme']) && ! isset($relUrl['host'])) {
        extract(parse_url($base));
        $path = preg_replace('#/[^/]*$#', '', $path);
        if ($rel[0] == '/')
            $path = '';
        $abs = "$host$path/" . $relUrl['path'];
        $re = array(
            '#(/\.?/)#',
            '#/(?!\.\.)[^/]+/\.\./#'
        );
        for ($n = 1; $n > 0; $abs = preg_replace($re, '/', $abs, - 1, $n)) {}
        $abs = str_replace("../", "", $abs);
        return $scheme . '://' . $abs;
    } else
        return $base;
}

/**
 * @param array $data
 * @param string $logFile
 * @desc logs links to the text file
 */
/*function logger(array $data, $logFile = "stack.log")
{
    $logFile = fopen($logFile, "w") or die("Unable to open file!");
    foreach ($data as $key => $value) {
        fwrite($logFile, $value . PHP_EOL);
    }
    fclose($logFile);
}*/

function get_domain($url)
{
  $pieces = parse_url($url);
  $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
    return $regs['domain'];
  }
  return false;
}
