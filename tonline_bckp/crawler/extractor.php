<?php


//this file takes html as an input, pulls the required values from the page and submits to data storage

$html = file_get_contents("spiegel/html/fcda3b767f2ccdd8e96dc275f299c52c.html");

//for spiegel
//breadcrumb = <div id="breadcrumb">
$result = array();

function getDataString($startString, $endString, $html){
	$startPos = strpos($html, $startString, strlen($startString));
	$endPos = strpos($html, $endString, $startPos);
	return trim(strip_tags(substr($html, $startPos, $endPos-$startPos)));
}


// BREADCRUMB
$startString = '<div id="breadcrumb">';
$startPos = strpos($html, $startString, strlen($startString));
$endString = '</div>';
$endPos = strpos($html, $endString, $startPos);

$array = explode("\n", strip_tags(substr($html, $startPos, $endPos-$startPos)));
$breadcrumb = array();
foreach($array as $element){
	$string = trim($element);
	if(strlen($string)>0)
		$breadcrumb[] = $string;
}
$result['breadcrumb'] = $breadcrumb;
//END BREADCRUMB



$headlineIntro = getDataString('<span class="headline-intro">', '</span>', $html);
$result['headlineIntro'] = $headlineIntro;

$headline = getDataString('<span class="headline">', '</span>', $html);
$result['headline'] = $headline;

$articleIntro = getDataString('<p class="article-intro">', '</p>', $html);
$result['articleIntro'] = $articleIntro;

$author = getDataString('<a class="autor-link js-author-link"', '</a>', $html);
$result['author'] = $author;

//GET TIME OF ARTICLE
$startString = 'datetime="';
$startPos = strpos($html, $startString);
$endPos = strpos($html, '">', $startPos);
$result['datetime'] = substr($html, $startPos+ strlen($startString), $endPos-(($startPos + strlen($startString))));

//GET CONTENT OF ARTICLE
//for this we have to get a subset of the article
$startString = '<time class="timeformat"';
$startPos = strpos($html, $startString);
$endPos = strpos($html, '<div class="module-box home-link-box">', $startPos);
$subHtml = substr($html, $startPos+ strlen($startString), $endPos-(($startPos + strlen($startString))));

$content = array();
$doc = new DOMDocument();
@$doc->loadHTML($subHtml);
foreach($doc->getElementsByTagName('p') as $p){
	$content[] = utf8_decode(trim($p->textContent))."\n";
}
array_shift($content);
$result['content'] = implode("\n",$content);

print_r($result);







