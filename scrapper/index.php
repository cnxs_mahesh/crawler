<?php

function fetchData($html1)
{
	libxml_use_internal_errors(TRUE); //disable libxml errors
	$html = file_get_contents($html1); 
	$glboalObj=new stdClass();
	$doc = new DOMDocument();
	$doc->loadHTML($html);
	$xpath = new DOMXPath($doc);
	$query = '//*/meta[starts-with(@property, \'og:\')]';
	$metas = $xpath->query($query);
	foreach ($metas as $meta) {
		$meta_array[$meta->getAttribute('property')]=$meta->getAttribute('content');
	}
	if($meta_array['og:type']=='article' && $meta_array['og:url']!== '!#canonical!#')
	{
		$glboalObj=new stdClass();
		$glboalObj->url=$meta_array['og:url'];
		$glboalObj->headline=$meta_array['og:title'];
		$glboalObj->shortdescription=$meta_array['og:description'];
		$glboalObj->img=$meta_array['og:image'];

		//scrapping the contend now
		$tags = $xpath->query('//*[@itemprop]');
		foreach ($tags as $tag) {
			if(isset($tag->tagName))
			{
				switch ($tag->tagName) {
					case 'meta':
								if($tag->getAttribute('itemprop')=="datePublished")
								{
									$glboalObj->datetime=$tag->getAttribute('content');
									$glboalObj->timestamp=strtotime($tag->getAttribute('content'));
								}
								//if($tag->getAttribute('itemprop')=="description")
								//{
								//	$GLOBALS['glboalObj']->description=$tag->getAttribute('content');
								//}
								break;
					case 'div':	if(isset($tag->childNodes->length) && $tag->childNodes->length)
								{
									$content='';
									foreach ($tag->childNodes as $allps) {
										if(isset($allps->tagName) && $allps->tagName=="p")
										{
											$content.=$allps->textContent." .";
											$allp_array[]=$allps->textContent;
										}
									}
									//$GLOBALS['glboalObj']->article=$allp_array;
									$glboalObj->content=$content;
									if(isset($allp_array[1]))
									{
										$glboalObj->intro_blurb=$allp_array[1];
									}
									
								}
								break;
				}
			}
		}

		$breadcrumb = $xpath->query("//div[@id='Tbcrumbinner']");
		foreach ($breadcrumb as $crumb) {
			if(isset($crumb->nodeValue))
			{
				$trail=$crumb->nodeValue;
			}
		}

		if(isset($trail))
		{
			$trail=str_replace("Sie sind hier: ","",$trail);
			$glboalObj->Breadcrumb=$trail;
		}
		echo "<pre>"; print_r($glboalObj); echo "</pre>";
		echo "<pre>Copy JSON:"; print_r(json_encode($glboalObj)); echo "</pre>";
	}
}

?>

